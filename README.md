 # openLooKeng

 openLooKeng is a distributed SQL query engine for big data.

 See the [User Manual](https://openlookeng.io/zh-cn/docs/docs/installation/deployment-auto.html) for deployment instructions and end user documentation.

- Linux OS like Cent OS
- Java 8 Update 151 or higher (8u151+), 64-bit. Both Oracle JDK and OpenJDK are supported.
- Maven 3.3.9+ (for building)
- Python 2.4+ (for running with the launcher script)
- See the [Installation package](https://openlookeng.io/download.html) for  installation
  1. hetu-server-1.6.0.tar.gz contains the files needed to run openLooKeng server.
  2. hetu-cli-1.6.0-executable.jar is an executable client program used to connect with the openLooKeng server, browse data directory information, configure session, submit query requests, etc.

##  Application Scenarios

### Cross-Source Heterogeneous Query Scenario

Data management systems like RDBMS (such as MySQL and Oracle) and NoSQL (such as HBase, ES, and Kafka) are widely used in various application systems of customers. With the increase of data volume and better data management, customers gradually build data warehouses based on Hive or MPPDB. These data storage systems are often isolated from each other, resulting in independent data islands. Data analysts often suffer from the following problems:

1. Unaware of where and how to use the data you want, you cannot build a new service model based on massive data.
2. Querying various data sources requires different connection modes or clients and running different SQL dialects. These differences cause extra learning costs and complex application development logic.
3. If data is not aggregated, federated query cannot be performed on data of different systems.

openLooKeng can be used to implement joint query of data warehouses such as RDBMS, NoSQL, Hive, and MPPDB. With the cross-source heterogeneous query capability of openLooKeng, data analysts can quickly analyze massive data.

### Cross-domain and cross-DC query

In a two-level or multi-level data center scenario, for example, a province-city data center or a headquarters-branch data center, users often need to query data from the provincial (headquarters) data center or municipal (branch) data center, the bottleneck of cross-domain query is the network problems (such as insufficient bandwidth, long delay, and packet loss) between multiple data centers. As a result, the query delay is long and the performance is unstable. openLooKeng is a cross-domain and cross-DC solution designed for cross-domain queries. The openLooKeng cluster is deployed in multiple DCs. After the openLooKeng cluster in DC2 completes computing, the result is transmitted to the openLooKeng cluster in DC1 through the network, aggregation calculation is completed in the openLooKeng cluster of DC1. In the cross-domain and cross-DC openLooKeng solution, calculation results are transmitted between openLooKeng clusters. This avoids network problems caused by insufficient network bandwidth and packet loss, and solves the cross-domain query problem to some extent.

### Separated Storage & Compute

openLooKeng itself does not have a storage engine, but it can query data stored in different data sources. Therefore, the system is a typical storage and computing separated system, which can easily expand the computing and storage resources independently. The openLooKeng storage and computing separation architecture is suitable for dynamic expansion clusters, facilitating quick elastic scaling of resources.

### Quick Data Exploration

Customers have a large amount of data. To use the data, they usually build a dedicated data warehouse. However, this will cause extra labor costs for data warehouse maintenance and data ETL time costs. For customers who need to quickly explore data but do not want to build a dedicated data warehouse, it is time-consuming and labor-intensive to replicate data and load the data to the data warehouse. openLooKeng can use standard SQL to define a virtual data mart and connect to each data source with the cross-source heterogeneous query capability. In this way, various analysis tasks that users need to explore can be defined at the semantic layer of the virtual data mart. With the data virtualization capability of openLooKeng, customers can quickly build exploration and analysis services based on various data sources without building complex and dedicated data warehouses.

## Key Features

### Cross Data Center Connector

A new connector is directly connected to another openLooKeng cluster to implement collaboration between multiple DCs. The key technologies are as follows:

1. Parallel data access: Data sources are concurrently accessed to improve access efficiency. Clients can concurrently obtain data from the server to accelerate data obtaining.
2. Data compression: Data is compressed using GZIP compression algorithm before being serialized during data transmission, reducing the amount of data transmitted over the network.
3. Cross-DC dynamic filtering: Filters data to reduce the amount of data to be pulled from the remote end, ensuring network stability and improving query efficiency.
4. High availability: In openLooKeng, Coordinator AA is supported. Therefore, you can use a proxy (for example, Nginx) to implement load balancing among Coordinators to achieve high availability.If a coordinator is faulty, the availability of the entire cluster is not affected.

### Dynamic Filtering

In the multi-table join scenario with low correlation, most probe side rows are filtered out because they do not match the join conditions after being read. As a result, unnecessary join calculation, I/O read, and network transmission are caused. Dynamic filtering dynamically generates filtering conditions based on join conditions and data read from the build-side table during query running, and applies the filtering conditions to the table scan phase of the probe-side table. This reduces the data volume of the probe table that participates in the join operation, effectively reducing network transmission and improving performance by 30%.

### Index

openLooKeng increases query efficiency by creating indexes on existing data and storing the indexes outside the data source. Bitmap is a bitmap index that records binary information. It is applicable to the AND operation and can be quickly queried using a dictionary. Bloom uses a bit array to represent a set, which can quickly determine whether a value (only the equal sign is supported) exists in a set. Min-max records the maximum and minimum values in a file, which is applicable to statements that are greater than or less than the condition.

### Cache

openLooKeng creates multiple caches such as metadata cache, execution plan cache, and ORC row data cache in order to improve query performance.

#### Metadata Cache

All Connectors that use JDBC connections cache metadata during the first query to improve the query performance.

#### ORC Row Data Cache

For ORC files, the ORC row data cache provides an efficient method to cache frequently accessed data to improve query latency. The administrator is able to create a cache on specific tables and partitions.

#### Execution Plan Cache

The execution plan is cached after the first query, rather than discarded after each query request, thereby reducing the preprocessing time and resources required for subsequent queries. By caching these plans, time-consuming plan generation steps can be skipped.

## Try-Me show

Experience using  [openLooKeng](https://tryme.openlookeng.io/) in a tangible way online.

Some demo test statements :

```sql
Cache: select count(*) from store_sales where store_sales.ss_sold_date_sk between 2451484 and 2451513;
```

```sql
INDEX: select count(*) from store_sales where ss_item_sk=123;
```

## Connect to multiple data sources

Connector is the method for openLooKeng to connect to external data sources. For specific configuration, please refer to [openLooKeng connector](https://openlookeng.io/zh-cn/docs/docs/connector.html).

## Building openLooKeng Core
openLooKeng Core is a standard Maven project. Simply run the following command from the project root directory:

```
./mvnw clean install
```

On the first build, Maven will download all the dependencies from the internet and cache them in the local repository (`~/.m2/repository`), which can take a considerable amount of time. Subsequent builds will be faster.

openLooKeng Core has a comprehensive set of unit tests that can take several minutes to run. You can disable the tests when building:

```
./mvnw clean install -DskipTests
```

## Running openLooKeng Core in your IDE
### Overview
After building openLooKeng Core for the first time, you can load the project into your IDE and run the server. We recommend using IntelliJ IDEA. Because openLooKeng is a standard Maven project, you can import it into your IDE using the root `pom.xml` file. In IntelliJ, choose Open Project from the Quick Start box or choose Open from the File menu and select the root `pom.xml` file.

After opening the project in IntelliJ, double check that the Java SDK is properly configured for the project:

- Open the File menu and select Project Structure

- In the SDKs section, ensure that a 1.8 JDK is selected (create one if none exist)

- In the Project section, ensure the Project language level is set to 8.0 as openLooKeng Core makes use of several Java 8 language features

openLooKeng Core comes with sample configuration that should work out-of-the-box for development. Use the following options to create a run configuration: 

- Main Class: `io.prestosql.server.PrestoServer`

- VM Options: `-ea -XX:+UseG1GC -XX:G1HeapRegionSize=32M -XX:+UseGCOverheadLimit -XX:+ExplicitGCInvokesConcurrent -Xmx2G -Dconfig=etc/config.properties -Dlog.levels-file=etc/log.properties`

- Working directory: `$MODULE_DIR$`

- Use classpath of module: `presto-main`

The working directory should be the `presto-main` subdirectory. In IntelliJ, using `$MODULE_DIR$` accomplishes this automatically.

In the config.properties file, replace plugin.bundles with plugin.dir pointing to your plugin folder

```
plugin.dir=../hetu-server/target/hetu-server-{VERSION}/plugin
```

 Additionally, the Hive plugin must be configured with location of your Hive metastore Thrift service. Add the following to the list of VM options, replacing `localhost:9083` with the correct host and port (or use the below value if you do not have a Hive metastore): 

```
-Dhive.metastore.uri=thrift://localhost:9083
```

### Using SOCKS for Hive or HDFS

If your Hive metastore or HDFS cluster is not directly accessible to your local machine, you can use SSH port forwarding to access it. Setup a dynamic SOCKS proxy with SSH listening on local port 1080:

```
ssh -v -N -D 1080 server
```

Then add the following to the list of VM options:

```
-Dhive.metastore.thrift.client.socks-proxy=localhost:1080
```

### Running the CLI

Start the CLI to connect to the server and run SQL queries:

```
presto-cli/target/hetu-cli-*-executable.jar
```

or like this :

```
java -jar ./hetu-cli-1.6.0-executable.jar --server localhost:8080 --catalog tpcds --schema tiny
```

Run a query to see the nodes in the cluster:

```sql
SELECT * FROM system.runtime.nodes;
```

In the sample configuration, the Hive connector is mounted in the `hive` catalog, so you can run the following queries to show the tables in the Hive database `default`:

```sql
SHOW TABLES FROM hive.default;
```

### Running the Web UI

The openLooKeng Web UI is accessible at the same address as the hetu server, using the same HTTP port number. By default, this port is 8080; 
So on your local installation, you can check out the Web UI at http://localhost:8080/ui/

Once openLooKeng is up and running. You connect to a data source and use SQL to query it. You can also use the oplk-CLI, or use JDBC to connect to openLooKeng's application.
## Obtaining the Design Document

Click [here](https://openlookeng.slite.com/p/channel/EDMAZKydV2MsM5trxJPmLv#) to obtain the design document.

## Release Notes

When authoring a pull request, the PR description should include its relevant release notes. 

Follow Release Notes Guidelines when authoring release notes.  

